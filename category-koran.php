<?php get_header(); ?>


<div class="container" style="padding-top: 20px;">
	<div class="col-xs-12" >
	<?php if (have_posts()) : while(have_posts()) : the_post(); ?>
	   	<div class="col-sm-6 col-md-3">
			<img style=" display: block; margin-left: auto; margin-right: auto;" src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id() ,'thumbnail'))[0]; ?>" width="198px" height="145px" />
		  	<div class="caption">
				 <p class="text-center"><strong><?php the_title(); ?></strong></p>
						 <p class="text-center"><?php the_excerpt() ?></p>
						 <p class="text-center">
							<a href="<?php echo get_post_meta( get_the_id(), 'link_koran', true ); ?>" class="btn btn-primary" role="button">
							   Download
							</a> 
							<a href="<?php the_permalink(); ?>" class="btn btn-default" role="button">
							   Preview
							</a>
						 </p>
					  </div>
				   </div>
	<?php endwhile; else: ?>
		<p><?php _e('No posts were found. Sorry!'); ?></p>
	<?php endif; ?>
	


				</div>
	<div class="navi">
		<div class="right">
			<?php previous_posts_link(); ?> -/- <?php next_posts_link(); ?>
		</div>
		<br>
	</div>
</div>




<?php get_footer(); ?>