<?php get_header(); ?>


<div class="container" style="padding-top: 20px;">
	<div class="col-xs-9">
	
	<?php if (have_posts()) : while(have_posts()) : the_post(); ?>
				<div class="post">
					<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
					<div class="byline">
						by <?php the_author_posts_link(); ?> 
						on 
						<?php the_time('l F d, Y') ?>
					</div>

	<hr class="style-eight">
					<?php the_content('Read More...') ?>
				</div>
				<!-- Next / Previous -->

			<?php endwhile; else: ?>
				<p><?php _e('No posts were found. Sorry!'); ?></p>
			<?php endif; ?>
			<div class="navi">
					<div class="right">
						<?php previous_posts_link(); ?> -/- <?php next_posts_link(); ?>
					</div>
					<br>
				</div>
	</div>
	<div class="col-md-4 col-lg-3">
		<?php get_sidebar(); ?>
			
	</div>
</div>




<?php get_footer(); ?>