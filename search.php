<?php
/*
Template Name: Search Page
*/
?>

<?php get_header(); ?>


<div class="container" style="padding-top: 20px;">
    <div class="col-xs-9">
    
    <?php if (have_posts()) : while(have_posts()) : the_post(); ?>
        <div class="media">
            <a class="pull-left" href="<?php echo the_permalink(); ?>">
                <img class="media-object" src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($latest[$ii]->ID ,'thumnail'))[0]; ?>" width="150px" height="90px" >
            </a>
            <div class="media-body">
                <small class="media-heading"><?php the_time('d/m/Y') ?> | by <?php the_author_posts_link(); ?> </small> <br>
                <a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a> <br>
                <?php the_excerpt(); ?>
            </div>
        </div>       
    <?php endwhile; else: ?>
        No thing found. <br>
    <?php endif; ?>
    </div>
    <div class="col-md-4 col-lg-3">
        <?php get_sidebar(); ?>
           
    </div>
</div>






<?php get_footer(); ?>