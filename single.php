<?php get_header(); ?>


<div class="container" style="padding-top: 20px;">
<div class="col-md-12 col-lg-12 in-row">
	<div class="col-md-12 col-lg-9">
	
	<?php if (have_posts()) : while(have_posts()) : the_post(); setPostViews(get_the_ID());?>
		

		<div class="post">
			<h3><?php the_title(); ?></h3>
			<div class="byline">
				by <?php the_author_posts_link(); ?> 
				on <span href="<?php the_permalink(); ?>">
				<?php the_time('l F d, Y') ?></span>
			</div>

			<hr>
			<?php the_content('Read More...') ?>
		</div>
		<!-- <?php the_tags(); ?> -->
		<!-- related post -->
		<?php 
			$tags = wp_get_post_tags(get_the_id());
			// jika ada tag
			if ($tags){
				$tags_id = array();
				foreach ($tags as $tag_ind) {
					$tags_id[] = $tag_ind->term_id;
				}
				$args = array(
					'tag__in' => $tags_id,
					'post__not_in' => array(get_the_id()),
					'showposts' => 4,
					'caller_get_posts' => 1
				);
				$mq = new wp_query($args);
				// jika ada related
				if ($mq->have_posts()){
					echo "Related Post <br><ul>";
					while($mq->have_posts()){
						$mq->the_post(); ?>

						<li><a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a></li>

					<?php }
				}
				wp_reset_query();
			}
		?>

		<!-- comment -->
		<div class="commentlist">
		<?php comments_template();?>
		</div>
		<?php endwhile; else: ?>
			<p><?php _e('No posts were found. Sorry!'); ?></p>
		<?php endif; ?>
	</div>
	<div class="col-md-4 col-lg-3">
		<?php get_sidebar(); ?>
			
	</div>
</div>
</div>




<?php get_footer(); ?>