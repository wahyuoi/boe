<div class="container">
	<div class="row">
		<div class="col-md-9">
			<div id="myCarousel" class="carousel slide">
				<div class="carousel-inner">
					<div class="active item">
						<div class="fill" style="background-image:url('http://www.portalapp.com/images/bg1.jpg');">
							<div class="container">
								<div class="blocky">
									<h1 class="pull-left pull-middle light-color"><span class=""><a href="#" class="">Lorem Ipsum</a></span></h1>
									<p class="pull-left pull-middle light-color text-padding"><span class="">Lorem ipsum dolor sit amet, 
										consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, 
										sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr </span>
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="fill" style="background-image:url('http://www.portalapp.com/images/bg1.jpg');">
							<div class="container">
								<div class="blocky">
									<h1 class="pull-left pull-middle light-color"><span class=""><a href="#" class="">Lorem Ipsum</a></span></h1>
									<p class="pull-left pull-middle light-color text-padding"><span class="">Lorem ipsum dolor sit amet, 
										consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, 
										sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr </span>
									</p>		
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="fill" style="background-image:url('http://www.portalapp.com/images/bg1.jpg');">
							<div class="container">
								<div class="blocky">
									<h1 class="pull-left pull-middle light-color"><span class=""><a href="#" class="">Lorem Ipsum</a></span></h1>
									<p class="pull-left pull-middle light-color text-padding"><span class="">Lorem ipsum dolor sit amet, 
										consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, 
										sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr </span>
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="fill" style="background-image:url('http://www.portalapp.com/images/bg1.jpg');">
							<div class="container">
								<div class="blocky">
									 <h1 class="pull-left pull-middle light-color"><span class=""><a href="#" class="">Lorem Ipsum</a></span></h1>
									<p class="pull-left pull-middle light-color text-padding"><span class="">Lorem ipsum dolor sit amet, 
										consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, 
										sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr </span>
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="fill" style="background-image:url('http://www.portalapp.com/images/bg1.jpg');">
							<div class="container">
								<div class="blocky">
									<h1 class="pull-left pull-middle light-color"><span class=""><a href="#" class="">Lorem Ipsum</a></span></h1>
									<p class="pull-left pull-middle light-color text-padding"><span class="">Lorem ipsum dolor sit amet, 
										consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, 
										sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr </span>
									</p>
								</div>
							</div>
						</div>
					</div>
					</div>	
					   <ul class="nav nav-pills nav-justified">
					<li data-target="#myCarousel" data-slide-to="0" class="active"><a href="#"><img src="http://s21.postimg.org/l180qcg6b/150x90.gif" alt="..."></a></li>
					<li data-target="#myCarousel" data-slide-to="1"><a href="#"><img src="http://s21.postimg.org/l180qcg6b/150x90.gif" alt="..."></a></li>
					<li data-target="#myCarousel" data-slide-to="2"><a href="#"><img src="http://s21.postimg.org/l180qcg6b/150x90.gif" alt="..."></a></li>
					<li data-target="#myCarousel" data-slide-to="3"><a href="#"><img src="http://s21.postimg.org/l180qcg6b/150x90.gif" alt="..."></a></li>
					<li data-target="#myCarousel" data-slide-to="4"><a href="#"><img src="http://s21.postimg.org/l180qcg6b/150x90.gif" alt="..."></a></li>
				</ul>
					<a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
					<a class="right carousel-control" href="#myCarousel" data-slide="next" contenteditable="false">›</a>
					 
			</div>
			<div id="push" class=""></div>
		</div>
		<div class="col-md-4 col-lg-3">
			<small><strong>Latest News</strong></small>
			<ul class="media-list main-list">
				<li class="media">
					<a class="pull-left" href="#">
						<img class="media-object" src="http://s21.postimg.org/l180qcg6b/150x90.gif" alt="...">
					</a>
					<div class="media-body">
						<small class="media-heading"><a href="#">Lorem ipsum dolor asit amet</a></small>
					</div>
				</li>
				<li class="media">
					<a class="pull-left" href="#">
						<img class="media-object" src="http://s21.postimg.org/l180qcg6b/150x90.gif" alt="...">
					</a>
					<div class="media-body">
						<small class="media-heading"><a href="#">Lorem ipsum dolor asit amet</a></small>
					</div>
				</li>
				<li class="media">
					<a class="pull-left" href="#">
						<img class="media-object" src="http://s21.postimg.org/l180qcg6b/150x90.gif" alt="...">
					</a>
					<div class="media-body">
						<small class="media-heading"><a href="#">Lorem ipsum dolor asit amet</a></small>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>