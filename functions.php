<?php 
	define('TEMPPATH', get_bloginfo('stylesheet_directory'));
	define('IMAGES', TEMPPATH."/images");
	define('JS', TEMPPATH."/js");

	add_theme_support( 'post-thumbnails' ); 
	add_theme_support( 'post-excerpt' ); 

	function custom_excerpt_length( $length ) {
		return 20;
	}
	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

	// menubar
	//if (function_exists('register_nav_menus')){
		register_nav_menus(
			array('home_menu' => 'Home Menu')
		);
	//}

	// sidebar
	// function BOE_widgets_init(){
		   /**
			* Creates a sidebar
			* @param string|array  Builds Sidebar based off of 'name' and 'id' values.
			*/
			$args = array(
				'name'          => __( 'Right Sidebar', 'BOE' ),
				'id'            => 'right-sidebar',
				'description'   => '',
				'class'         => '',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '<center><h4 class="widgettitle">',
				'after_title'   => '</h4></center>'
			);
		
			register_sidebar( $args );
	// }
	// add_action('widgets_init','BOE_widgets_init' );

	function getPostViews($postID){
	    $count_key = 'post_views_count';
	    $count = get_post_meta($postID, $count_key, true);
	    if($count==''){
	        delete_post_meta($postID, $count_key);
	        add_post_meta($postID, $count_key, '0');
	        return "0 View";
	    }
	    return $count.' Views';
	}
	function setPostViews($postID) {
	    $count_key = 'post_views_count';
	    $count = get_post_meta($postID, $count_key, true);
	    if($count==''){
	        $count = 0;
	        delete_post_meta($postID, $count_key);
	        add_post_meta($postID, $count_key, '0');
	    }else{
	        $count++;
	        update_post_meta($postID, $count_key, $count);
	    }
	}
?>
