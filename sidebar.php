<small><strong>Latest News</strong></small>
<ul class="media-list main-list">
<?php 
	$posts_per_page = 4; //get_option('posts_per_page' );
	$latest = get_posts( array('posts_per_page' => $posts_per_page) );
	foreach ($latest as $post) { ?>
		<li class="media">
			<a class="pull-left" href="<?php echo get_permalink($post->ID); ?>">
				<img class="media-object" src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'thumbnail')[0]; ?>" width='100px' height='65px'>
			</a>
		<div class="media-body">
		<small class="media-heading"><a href="<?php echo get_permalink($post->ID ); ?>"><?php echo $post->post_title; ?></a></small>
		</div>
	</li>
<?php } ?>				
</ul>

<div class="">
	<small><strong>Koran BOE</strong></small>
	<ul class="media-list main-list">
	<?php 
		$posts_per_page = get_option('posts_per_page' );
		$latest = get_posts( array('posts_per_page' => 1, 'category' => get_cat_id('koran')) );
		foreach ($latest as $post) { ?>
			<li class="media">
				<a class="pull-left" href="<?php echo get_permalink($post->ID); ?>">
					<img class="media-object" src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'thumbnail')[0]; ?>" width='100px' height='65px'>
				</a>
				<div class="media-body">
					<small class="media-heading"><a href="<?php echo get_permalink($post->ID ); ?>"><?php echo $post->post_title; ?></a></small>
				</div>
				<div class="btn-group">
					<a href="<?php echo get_post_meta( $post->ID, 'link_koran', true ); ?>" class="btn btn-xs btn-success">Download</a>
					<a href="<?php echo get_permalink($post->ID); ?>" class="btn btn-xs btn-warning">Preview</a>
				</div>
			</li>
		<?php }
	?>
		
	</ul>
</div>

<div class="">
	<small><strong>Majalah BOE</strong></small>
	<ul class="media-list main-list">
	<?php 
		$posts_per_page = get_option('posts_per_page');
		$latest = get_posts( array('posts_per_page' => 1, 'category' =>  get_cat_id('majalah')) );
		foreach ($latest as $post) { ?>
			<li class="media">
				<a class="pull-left" href="<?php echo get_permalink($post->ID); ?>">
					<img class="media-object" src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'thumbnail')[0]; ?>" width='100px' height='65px'>
				</a>
				<div class="media-body">
					<small class="media-heading"><a href="<?php echo get_permalink($post->ID ); ?>"><?php echo $post->post_title; ?></a></small>
				</div>
				<div class="btn-group">
					<a href="<?php echo get_post_meta( $post->ID, 'link_majalah', true ); ?>" class="btn btn-xs btn-success">Download</a>
					<a href="<?php echo get_permalink($post->ID); ?>" class="btn btn-xs btn-warning">Preview</a>
				</div>
			</li>
		<?php }
	?>
		
	</ul>
</div>

<div class="">
	<small><strong>Popular Post</strong></small>
	<ul class="media-list main-list">
		<?php
			query_posts('meta_key=post_views_count&orderby=meta_value_num&order=DESC');
			if (have_posts()) : while (have_posts()) : the_post(); ?>
				<li class="media">
						<a class="pull-left" href="<?php echo get_permalink($post->ID); ?>">
							<img class="media-object" src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'thumbnail')[0]; ?>" width='100px' height='65px'>
						</a>
						<div class="media-body">
							<small class="media-heading"><a href="<?php echo get_permalink($post->ID ); ?>"><?php echo $post->post_title; ?></a></small>
						</div>
				</li>
			<?php
			
			endwhile; endif;
			wp_reset_query();
		?>
	</ul>
</div>
		<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('right-sidebar')) : ?><?php endif; ?>
		
<!-- 		<div class="span8" style="padding-bottom: 20px;">
			<div class="flex-video widescreen" style="margin: 0 auto;text-align:center;">
				<iframe allowfullscreen="" src="http://www.youtube.com/embed/6NbAAmDuv_8?feature=player_detailpage" frameborder="0"></iframe>
			</div>
		</div>
		<div class="span8"style="padding-bottom: 20px;">
			<div class="flex-video widescreen" style="margin: 0 auto;text-align:center;">
				<iframe allowfullscreen="" src="http://www.youtube.com/embed/6NbAAmDuv_8?feature=player_detailpage" frameborder="0"></iframe>
			</div>
		</div> -->