<?php get_header(); ?>

<div class="container" style="padding-top: 20px;">
<div class="row in-row">
<div class="col-md-12">
	<div class="col-md-8 col-lg-9 in-row">
		<div id="myCarousel" class="carousel slide">
				<div class="carousel-inner">
					<?php 
						$sticky = get_option('sticky_posts' );
						rsort($sticky);

						// var_dump($sticky);
						for ($ii=0; $ii < 5; $ii++) { 
							$cur = get_post( $sticky[$ii] );
							?>
							<div class="<?php if ($ii==0) echo 'active '; ?> item">
								<div class="fill" style="background-image:url('<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($sticky[$ii]), 'full' )[0] ?>');">
									<div class="carousel-label">
											<h1 class="pull-left pull-middle light-color"><span class=""><a href="<?php echo get_permalink($sticky[0] ); ?>" class=""><?php echo $cur->post_title; ?></a></span></h1>
											<?php if ($cur->post_excerpt != '') { ?><p class="pull-left pull-middle light-color text-padding"><span class=""><?php echo $cur->post_excerpt; ?> </span> <?php } ?>
											</p>
									</div>
								</div>
							</div>		
						<?php }
					?>
					
				</div>	
			    <ul class="nav nav-pills nav-justified nav-slider">
			    <?php 
			    	for ($ii=0; $ii < 5; $ii++) { 
			    		$cur = get_post($sticky[$ii]); 
			    		?>
							<li data-target="#myCarousel" data-slide-to="<?php echo $ii; ?>" class="<?php if($ii==0) echo 'active'; ?>">
								<a href="#">
									<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($sticky[$ii]), 'thumbnail' )[0] ?>" 
										width='100px' height='65px'>
								</a>
							</li>
		
			    <?php	}
			    ?>
				</ul>
				<!--
				<a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
				<a class="right carousel-control" href="#myCarousel" data-slide="next" contenteditable="false">›</a>
				--> 
		</div>
		<div id="push" class=""></div>
		<?php 
			$jj=0;
			$categories = get_categories(array('hide_empty' => 1));
			foreach ($categories as $cat) { 
					$latest = get_posts(array('posts_per_page' => $posts_per_page, 'category' => $cat->term_id));
					//if ($jj%2==0)
						//echo "<div>";
				?>
				<div class="col-md-6 col-lg-6 min-row">
					<ul class="media-list main-list">
						<!-- image -->
						<li>
						<div class="featured-category">
								<img alt="" src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($latest[0]->ID ,'thumbnail'))[0]; ?>" width="198px" height="145px" />
								<span class="label label-inverse category-label photo-label-<?php echo $jj ?>"><a style="color: #ffffff;" href="<?php echo get_category_link($cat->term_id); ?>"><?php echo $cat->name; ?></a></span>
								<div class="description-featured">
									<a href="<?php echo get_permalink($latest[0]->ID); ?>"><?php echo $latest[0]->post_title; ?></a>
									<br><div class="time"><?php the_time('d/m/Y    h:i','',''); ?></div>
								</div>
						</div>
						<small><a href="<?php echo get_permalink($latest[0]->ID); ?>"><?php echo $latest[0]->post_excerpt; ?></a></small>
								<!-- end description div -->
							<!-- end wrapper div -->
						<li>
					</ul>
					<ul class="media-list main-list">
						<?php 
							for ($ii=1; $ii < 4; $ii++) { ?>
								<li class="media">
									<a class="pull-left" href="<?php echo get_permalink($latest[$ii]->ID); ?>">
										<img class="media-object" src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($latest[$ii]->ID ,'thumnail'))[0]; ?>" width="150px" height="90px" >
									</a>
									<div>
										<small class="media-heading"><a href="<?php echo get_permalink($latest[$ii]->ID); ?>"><?php echo $latest[$ii]->post_title; ?></a></small>
										<br><div class="time"><?php the_time('d/m/Y    h:i','',''); ?></div>
									</div>
								</li>		
							<?php }
						?>
						
					</ul>
				</div>		
			<?php 
				//if ($jj++%2)
				//		echo "</div>";
				++$jj;
			}
		?>
	</div>
	<div class="col-md-4 col-lg-3">
		<?php get_sidebar(); ?>		
	</div>
</div>
</div>
</div>




<?php get_footer(); ?>