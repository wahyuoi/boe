</body>
	  <footer>
      <div class="container">
        <div class="row">
			<div class="col-xs-3">
				<p class="pvl">
					<div >
						<img style="float: center; width: 110%; height: 110%;" src="http://boeconomica.org/wp-content/uploads/2013/11/boelogo.png" alt="BOE FEUI">
					</div>
				</p>
				<p>Student Center Lt.1 Kampus FEUI Depok 16424 Telepon/fax: (021)7865984</p>
				<p><i>Copyright © 2014 Badan Otonom Economica Fakultas Ekonomi Universitas Indonesia</i></p>
			</div> <!-- /col-xs-7 -->
			<div class="col-xs-8">
				<div class="footer-banner">
					<h3 class="footer-title">Contact Us</h3>
					<ul>
						<li>Student Center Lt.1 Kampus FEUI Depok 16424 </li>
						<li>Telepon/fax: (021)7865984</li>
					</ul>
				</div>
			</div>
			<!-- /icon font row -->
        </div>
      </div>
    </footer>

    <!-- Load JS here for greater good =============================-->
    <script src="<?php print JS; ?>/jquery-1.8.3.min.js"></script>
    <script src="<?php print JS; ?>/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="<?php print JS; ?>/jquery.ui.touch-punch.min.js"></script>
    <script src="<?php print JS; ?>/bootstrap.min.js"></script>
    <script src="<?php print JS; ?>/bootstrap-select.js"></script>
    <script src="<?php print JS; ?>/bootstrap-switch.js"></script>
    <script src="<?php print JS; ?>/flatui-checkbox.js"></script>
    <script src="<?php print JS; ?>/flatui-radio.js"></script>
    <script src="<?php print JS; ?>/jquery.tagsinput.js"></script>
    <script src="<?php print JS; ?>/jquery.placeholder.js"></script>
		
	<?php wp_footer(); ?>
	
  </body>
</html>