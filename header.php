<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
			<title><?php bloginfo('name'); ?> | <?php wp_title(); ?></title>
			<link rel="shortcut icon" href=<?php bloginfo('template_url'); ?>/11/favicon.png">
			<!-- Loading Bootstrap -->
			<link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet">
			
			
			<!-- Loading Flat UI -->
			<script src="<?php bloginfo('stylesheet_url'); ?>/js/jquery.min.js"></script>
			<link href="<?php bloginfo('stylesheet_url'); ?>css/jquerysctipttop.css" rel="stylesheet" type="text/css">
			
			<script src="<?php bloginfo('template_url'); ?>/js/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
			<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
			<!--[if lt IE 9]>
			<script src="js/html5shiv.js"></script>
			<script src="js/respond.min.js"></script>
			<![endif]-->
			<?php wp_head(); ?>
	</head>
	<body>
	<!--
		<div class="navbar navbar-default navbar-fixed-top" role="navigation">
		  <div class="container">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="<?php echo site_url(); ?>">Badan Otonom Economica</a>
			</div>
			<div class="navbar-collapse collapse">
			  <form class="navbar-form navbar-left" role="search">
				<div class="form-group">
				  <input type="text" class="form-control" placeholder="Search" name="s">
				</div>
				<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
			  </form>
			  <ul class="nav navbar-nav navbar-right">
				<a href="http://www.facebook.com/"><img style="float: center; width: 15%; height: 15%;" src="http://s17.postimg.org/sxj2x1byz/image.png" alt="Facebook"></a>
				<a href="http://www.twitter.com/"><img style="float: center; width: 15%; height: 15%;" src="http://s27.postimg.org/nhcb3a5zz/twitter.png" alt="Twitter"></a>
				<a href="http://www.youtube.com/"><img style="float: center; width: 15%; height: 15%;" src="http://s15.postimg.org/vafu9co5z/youtube.png" alt="Youtube"></a>
				<a href="http://www.linkedin.com/"><img style="float: center; width: 15%; height: 15%;" src="http://s12.postimg.org/8qct4k421/image.png" alt="LinkedIn"></a>			
			  </ul>
			</div>
		  </div>
		</div>
	-->
		<div class="container">
			<div class="row" id="header">
				<div class="col-md-4" id="left-header">
					<div id="date">
						<?php echo date("l, d F Y");?>
					</div>
					<div id="social">
						<a href="http://www.facebook.com/"><img style="float: center; width: 15%; height: 15%;" src="http://s17.postimg.org/sxj2x1byz/image.png" alt="Facebook"></a>
						<a href="http://www.twitter.com/"><img style="float: center; width: 15%; height: 15%;" src="http://s27.postimg.org/nhcb3a5zz/twitter.png" alt="Twitter"></a>
						<a href="http://www.youtube.com/"><img style="float: center; width: 15%; height: 15%;" src="http://s15.postimg.org/vafu9co5z/youtube.png" alt="Youtube"></a>
						<a href="http://www.linkedin.com/"><img style="float: center; width: 15%; height: 15%;" src="http://s12.postimg.org/8qct4k421/image.png" alt="LinkedIn"></a>
					</div>
				</div>
				<div class="col-md-4" id="header-image"><img style="float: center;" src="<?php bloginfo('template_url'); ?>/images/logoboe.png" alt="BOE FEUI"></div>
				<div class="col-md-4" id="right-header">
					<div class="navbar-collapse collapse" id="search">
					  <form class="navbar-form navbar-left" role="search">
						<div class="form-group">
						  <input type="text" class="form-control" placeholder="Search" name="s" id="s">
						</div>
						<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
					  </form>
					</div>
				</div>			
			</div>
			<div class="row in-row" id="menu">
				<nav class="navbar navbar-inverse navbar-custom" role="navigation">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
					  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-9">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					  </button>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="navbar-collapse collapse" id="bs-example-navbar-collapse-9">
					  <ul class="nav navbar-nav">
						<?php wp_nav_menu(
							array(
								'container' => '',
								'fallback_cb' => false,
								'theme_location' => 'home_menu',
								'items_wrap' => '%3$s',
								'before' => '',
								'after'  => ''
							)
						); ?>
					  </ul>
					</div><!-- /.navbar-collapse -->
				</nav>
			</div>
			
		</div>
		<!-- /.container -->