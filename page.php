<?php get_header(); ?>


<div class="container" style="padding-top: 20px;">
<div class="col-md-12 col-lg-12 in-row">
    <div class="col-md-8 col-lg-9">
    
    <?php if (have_posts()) : while (have_posts()) : the_post();?>
    <div class="post">
        <h2 id="post-<?php the_ID(); ?>"><?php the_title();?></h2>
        <div class="entrytext">
            <?php the_content('<p class="serif">Read the rest of this page »</p>'); ?>
        </div>
    </div>
    <?php endwhile; endif; ?>
    </div>
    <div class="col-md-4 col-lg-3">
        <?php get_sidebar(); ?>
           
    </div>
</div>
</div>






<?php get_footer(); ?>